import nash
import numpy as np
import sys

#first = [[-100, -50, 0], [-15, -45, -115]]
#second = [[1, 8, -9], [2, 0, -1], [-6, -4, 5], [-2, 2, 0]]

#C = np.array([[1, 8, -9], [2, 0, -1], [-6, -4, 5], [-2, 2, 0]])

#min_value = abs(np.min(C) - 1)
#print(min_value)
#C = C + min_value
#print(C)

#exam = [[3, 9, -4, 3, -4], [10, -2, 3, 9, -3], [3, 0, -2, -5, 8], [-4, 1, 0, 0, 1]]
exam = [[10, -90, 40, 60], [40, -30, 60, 80], [-60, 80, -50, 60]]

def solve(matrix):
    game = nash.Game(matrix)
    p, q = next(game.vertex_enumeration())
    print(matrix)
    print(np.round(p, 3))
    print(np.round(q, 3))
    print(round(np.dot(np.dot(p, matrix), q), 2))

if __name__ == '__main__':
    solve(exam)
