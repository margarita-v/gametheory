from operator import add, neg

def solve(payoff_matrix, iterations=1000):
    transpose = zip(*payoff_matrix)

    numrows = len(payoff_matrix)
    numcols = len(transpose)

    row_cum_payoff = [0] * numrows
    col_cum_payoff = [0] * numcols
    
    colpos = range(numcols)
    rowpos = map(neg, xrange(numrows))

    colcnt = [0] * numcols
    rowcnt = [0] * numrows
    active = 0

    for i in xrange(iterations):
        rowcnt[active] += 1        
        col_cum_payoff = map(add, payoff_matrix[active], col_cum_payoff)

        active = min(zip(col_cum_payoff, colpos))[1]
        colcnt[active] += 1       

        row_cum_payoff = map(add, transpose[active], row_cum_payoff)
        active = -max(zip(row_cum_payoff, rowpos))[1]

    value_of_game = (max(row_cum_payoff) + min(col_cum_payoff)) / 2.0 / iterations
    return rowcnt, colcnt, value_of_game

print solve([[-100, 0, -50], [-15, -115, -45]])
#print solve([[16, 116, 66], [101, 1, 71]])
#print solve([[1, 8, -9], [2, 0, -1], [-6, -4, 5], [-2, 2, 0]])
