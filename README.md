# Useful links

[Amazing online calculator](https://www.math.ucla.edu/~tom/gamesolve.html)

[Another solution](http://code.activestate.com/recipes/496825-game-theory-payoff-matrix-solver/)

[Medium article](https://medium.com/@adamnovo/linear-programming-in-python-cvxopt-and-game-theory-8626a143d428)